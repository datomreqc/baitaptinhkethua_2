package BaitapTinhKeThua;

import java.util.Scanner;

public class Sach extends Document{
    private String tenTacGia;
    private int soTrang;
    private double giaSach;

    public Sach(Scanner sc){
        super();
    }

    public Sach(String tenTacGia, int soTrang,double giaSach) {
        this.tenTacGia = tenTacGia;
        this.soTrang = soTrang;
        this.giaSach =  giaSach;
    }

    public Sach(String maTaiLieu, String tenNhaXuatBan, int soBanPhatHanh, String tenTacGia, int soTrang) {
        super(maTaiLieu, tenNhaXuatBan, soBanPhatHanh);
        this.tenTacGia = tenTacGia;
        this.soTrang = soTrang;
    }

    public String getTenTacGia() {
        return tenTacGia;
    }

    public int getSoTrang() {
        return soTrang;
    }

    public double getGiaSach() {
        return giaSach;
    }

    public void setTenTacGia(String tenTacGia) {
        this.tenTacGia = tenTacGia;
    }

    public void setSoTrang(int soTrang) {
        this.soTrang = soTrang;
    }

    public void setGiaSach(double giaSach) {
        this.giaSach = giaSach;
    }

    public String toString() {      // in Sach class
        String thongtin =  "Sách: " + super.toString()  // use Document toString()
                + " Tên tác giả: " + tenTacGia+" "+" Số Trang: "+soTrang+" "+ "Giá sách: "+ giaSach;
        return thongtin;
    }
    public void nhapThongTin(Scanner sc) {
        super.nhapThongTin(sc);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Giá Sách");
        this.setGiaSach(sc.nextDouble());
        System.out.println("Số trang sách là: ");
        this.setSoTrang(Integer.parseInt(scanner.nextLine()));
    }
}
